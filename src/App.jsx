import { useEffect, useState } from 'react';
import { Button, Modal, Header } from './components';
import './App.scss';
import ProductList from './components/product_list/ProductList.jsx';
import fetchData from './tools/fetchData';

const App = () => {

    const [isOpenModal, setIsOpenModal] = useState(false);
    const [products, setProducts] = useState([]);
    const [cart, setCart] = useState(JSON.parse(localStorage.getItem('cart')) || []);
    const [favorite, setFavorite] = useState(JSON.parse(localStorage.getItem('favorite')) || []);
    const [targetProductID, setTargetProductID] = useState('');
    const [modalParams, setModalParams] = useState({
        closeButton: false,
        headerTxt: '',
        bodyTxt: [],
        modalClassName: '',
    });

    useEffect(() => {
        console.log('effect_1');
        (async function getData() {
            setProducts(await fetchData());
        })()
    }, []);

    useEffect(() => {
        console.log('effect_2');
        localStorage.cart = JSON.stringify(cart);
        localStorage.favorite = JSON.stringify(favorite);
    }, [cart, favorite]);

    const openModal = () => setIsOpenModal(true);
    const closeModal = () => setIsOpenModal(prevState => prevState = false);

    const openAddToCartModal = (title, id) => {
        setTargetProductID(id);
        setModalParams(prevState => prevState = {
            ...prevState,
            closeButton: true,
            headerTxt: title,
            bodyTxt: [`Бажаєте додати '${title}'`, `до кошика?`],
            modalClassName: 'addToCartModal',
        })
        openModal();
    }

    const addProductsToFavorite = (action, id) => {
        action === '+' && products.forEach(product => product.id === id && setFavorite(prevState => prevState = [...prevState, product]));
        action === '-' && setFavorite(favorite.filter(product => product.id !== id));
    }

    const addProductsToCart = () => {
        products.forEach(product => product.id === targetProductID && setCart(prevState => prevState = [...prevState, product]))
    }

    const dellProductFromCart = (id) => setCart(cart.filter(product => product.id !== id));

    return (
        <div className="App">
            <Header cart={cart} favorite={favorite} dellProductFromCart={dellProductFromCart} />
            <div className="container">
                <ProductList
                    products={products}
                    favorite={favorite}
                    cart={cart}
                    openAddToCartModal={openAddToCartModal}
                    addProductsToFavorite={addProductsToFavorite}
                />
                {!!isOpenModal &&
                    <Modal
                        closeModal={closeModal}
                        {...modalParams}
                        actions={[
                            <Button closeModal={closeModal} className={modalParams.modalClassName} onClickFunc={addProductsToCart}>OK</Button>,
                            <Button closeModal={closeModal} className={modalParams.modalClassName}>Скасувати</Button>
                        ]}
                    />
                }
            </div>
        </div >
    )
}

export default App;
