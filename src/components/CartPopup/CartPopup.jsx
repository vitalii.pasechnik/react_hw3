import { useCallback, useEffect, useMemo, useState } from 'react';
import classes from './CartPopup.module.scss';
import Button from '../Button/Button';
import PropTypes from 'prop-types';

const CartPopup = ({ dellProductFromCart, cart, handleToggleCartPopup }) => {

    const mergeProducts = (arr) => {
        console.log('merge');
        return arr.reduce((acc, el) => {
            const dublicat = acc.findIndex(a => a.id === el.id);
            dublicat > -1 ? acc[dublicat].count++ : acc.push({ count: 1, ...el });
            return acc;
        }, [])
    };

    const mergedProducts = useMemo(() => mergeProducts(cart), [cart]);

    const cartTotalPrice = useMemo(() => {
        console.log('calc');
        return mergedProducts.reduce((acc, el) => acc += el.count * (el.price.sale || el.price.common), 0)
    }, [cart]);

    return (
        <>
            <div className={classes.cart_mini}>
                <ul className={classes.product_list} >
                    {mergedProducts.map((product) =>
                        <li className={classes.product_row} key={product.id}>
                            {!!product.count && <span>{product.count} x</span>}
                            <span>{product.title}</span>
                            <span>{!!product.price.sale ? product.price.sale : product.price.common} грн</span>
                            <div className={classes.closeBtn} onClick={() => dellProductFromCart(product.id)}></div>
                        </li>
                    )}
                    <li><strong>Загальна сума: {cartTotalPrice} грн</strong></li>
                </ul>
                <Button backgroundColor='rgba(255, 255, 255, 0.25)' className='enterCartBtn'>Перейти до кошика</Button>
            </div >
            <div className={classes.layout} onClick={() => handleToggleCartPopup()}></div>
        </>
    )
}

CartPopup.propTypes = {
    dellProductFromCart: PropTypes.func.isRequired,
    handleToggleCartPopup: PropTypes.func.isRequired,
    cart: PropTypes.arrayOf(PropTypes.object).isRequired
}

export default CartPopup;