import { useCallback, useEffect, useState } from 'react';
import classes from './Header.module.scss';
import { ReactComponent as CartSvg } from '../../img/cart-shopping-solid.svg';
import { ReactComponent as FavSvg } from '../../img/heart-solid.svg';
import CartPopup from '../CartPopup/CartPopup';
import PropTypes from 'prop-types';

const Header = ({ favorite, cart, dellProductFromCart }) => {

    const [isOpenCartPopup, setIsOpenCartPopup] = useState(false);

    const handleToggleCartPopup = () => !!cart.length && setIsOpenCartPopup(prevState => !prevState);

    useEffect(() => {
        !cart.length && setIsOpenCartPopup(false)
        console.log('effect');
    }, [cart])

    return (
        <div className={classes.header}>
            <div className={classes.container}>
                <div className={classes.fav_wrapper}>
                    <FavSvg className={classes.heart} />
                    {!!favorite.length && <div className={classes.count}>{favorite.length}</div>}
                </div>
                <div className={classes.cart_wrapper} onClick={handleToggleCartPopup}>
                    <CartSvg className={classes.cart} />
                    {!!cart.length && <div className={classes.count}>{cart.length}</div>}
                </div>
                {!!isOpenCartPopup && !!cart.length &&
                    < CartPopup cart={cart} handleToggleCartPopup={handleToggleCartPopup} dellProductFromCart={dellProductFromCart} />
                }
            </div>
        </div>
    )
}

Header.propTypes = {
    favorite: PropTypes.arrayOf(PropTypes.object).isRequired,
    cart: PropTypes.arrayOf(PropTypes.object).isRequired,
    dellProductFromCart: PropTypes.func.isRequired,
}

export default Header;