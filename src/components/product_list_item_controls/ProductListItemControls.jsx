import React, { useEffect, useState } from "react";
import classes from './ProductListItemControls.module.scss';
import { ReactComponent as BasketSVG } from '../../img/basket_icon.svg';
import Button from "../Button/Button";
import cn from 'classnames';
import PropTypes from 'prop-types';

const ProductListItemControls = ({ price, openAddToCartModal, id, title, available, cart }) => {
    const [discount, setDiscount] = useState(null);

    useEffect(() => {
        console.log('controlls');
        if (!!price.sale) {
            if (price.common > 100) {
                setDiscount(`${Math.round((price.sale - price.common) * 100) / 100} грн`)
            } else if (price.common <= 100) {
                setDiscount(`${Math.round(100 * price.sale / price.common) - 100}%`)
            }
        }
    }, [])

    const markProductCartBtn = () => cart.some((product) => product.id === id)

    return (
        <div className={classes.product_list_item_controls}>
            {!!available ?
                <>
                    <div className={classes.product_price_container}>
                        <div className={classes.sale_price}>
                            {!!price.sale ? price.sale : price.common}
                            <span className={classes.price_currency}> грн</span>
                        </div>
                        <div className={classes.old_price}>
                            <div className={classes.old_integer}>{!!price.sale && price.common}</div>
                            <div className={classes.discount}>{discount}</div>
                        </div>
                    </div>
                    <button
                        className={cn(classes.add_to_cart_round, !!markProductCartBtn() && classes.added)}
                        title="Додати в кошик"
                        onClick={() => openAddToCartModal(title, id)}
                    >
                        <BasketSVG className={classes.icon_basket} />
                    </button>
                </>
                :
                <div className={classes.add_to_comment_wrapper}>
                    <div className={classes.btn_tooltip} role="tooltip">
                        <span>Додамо до замовлення, якщо з’явиться до моменту видачі</span>
                    </div>
                    <Button className='productItem_button'>Хочу замовити</Button>
                </div>
            }
        </div>
    )
}

ProductListItemControls.propTypes = {
    available: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    price: PropTypes.object.isRequired,
    cart: PropTypes.arrayOf(PropTypes.object).isRequired,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    openAddToCartModal: PropTypes.func,
}

export default ProductListItemControls;




