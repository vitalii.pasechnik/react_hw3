import { Fragment } from 'react';
import classes from './Modal.module.scss';
import cn from 'classnames';
import PropTypes from 'prop-types';


const Modal = ({ headerTxt, bodyTxt, closeButton, actions, closeModal, modalClassName }) => {

    return (
        <>
            <div className={cn(classes.modal, classes[modalClassName])}>
                <div className={classes.modal__header}>
                    {headerTxt}
                    {!!closeButton && <div className={classes.closeBtn} onClick={closeModal}></div>}
                </div>
                <div className={classes.modal__body}>
                    {!!bodyTxt && bodyTxt.map((string, i) => <span key={i}>{string}</span>)}
                </div>
                {!!actions &&
                    <div className={classes.modal__actions}>
                        {actions.map((el, i) => <Fragment key={i}>{el}</Fragment>)}
                    </div>}
            </div>
            <div className={cn(classes.modal__layout, classes[modalClassName])} onClick={closeModal}></div>
        </>
    )
}

Modal.propTypes = {
    headerTxt: PropTypes.string,
    bodyTxt: PropTypes.arrayOf(PropTypes.string),
    actions: PropTypes.arrayOf(PropTypes.node),
    closeButton: PropTypes.bool.isRequired,
    closeModal: PropTypes.func.isRequired,
    modalClassName: PropTypes.string.isRequired,
}

export default Modal;