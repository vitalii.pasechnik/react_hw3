import classes from './ProductList.module.scss';
import ProductListItem from "../product_list_item/ProductListItem";
import PropTypes from 'prop-types';

const ProductList = ({ products, ...props }) => {

    return (
        <ul className={classes.product_list} >
            {products.map((product) =>
                <ProductListItem key={product.id} {...product} {...props} />
            )}
        </ul>
    )
}

ProductList.propTypes = {
    products: PropTypes.arrayOf(PropTypes.object).isRequired
}

export default ProductList;