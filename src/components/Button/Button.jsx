import classes from './Button.module.scss';
import cn from 'classnames';
import PropTypes from 'prop-types';

const Button = ({ children, backgroundColor = '', closeModal, className, onClickFunc }) => {

    return (
        <button
            onClick={() => {
                !!closeModal && closeModal();
                !!onClickFunc && onClickFunc();
            }}

            className={cn(classes.button, classes[className])}
            style={{ backgroundColor: backgroundColor }}
        >
            {children}
        </button >
    )
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    onClickFunc: PropTypes.func,
    closeModal: PropTypes.func,
    className: PropTypes.string.isRequired,
    children: PropTypes.string.isRequired,
}

export default Button;