import classes from './ProductListItem.module.scss';
import { ReactComponent as StarHolderSVG } from '../../img/star_holder.svg';
import { ReactComponent as StarHolderZeroSVG } from '../../img/star_holder_zero.svg';
import { ReactComponent as FavSvg } from '../../img/heart-solid.svg';
import cn from 'classnames';
import ProductListItemControls from "../product_list_item_controls/ProductListItemControls";
import PropTypes from 'prop-types';

const ProductListItem = (props) => {

    const { addProductsToFavorite, id, favorite, available, img, title, rating, value } = props;
    const default_img = 'https://shop.silpo.ua/images/silpo/placeholders/4.svg';

    const markProductFavBtn = () => favorite.some((product) => product.id === id);

    const addToFav = () => {
        !markProductFavBtn() ? addProductsToFavorite('+', id) : addProductsToFavorite('-', id);
    }

    return (
        <li className={`${classes.product_list_item_wrapper} ${!available && classes.not_available}`}>
            <div className={classes.product_list_item}>
                <div className={classes.product_list_item_header}>
                    <button
                        className={cn(classes.add_to_favourite, !!markProductFavBtn() && classes.added)}
                        title="Додати до обронного"
                        onClick={() => addToFav()}
                    >
                        <FavSvg className={classes.heart} />
                    </button>
                </div>
                <div className={classes.image_content_wrapper} >
                    <a href={`${img || default_img}`} target="_blank" >
                        <img className={classes.product_list_item__image} src={!!img ? img : default_img} alt={title} />
                    </a>
                    <div className={classes.product_list_item__content}>
                        <div className={classes.product_title}>{title}</div>
                        <div className={classes.rating_wrapper}>
                            {!!rating.votes ?
                                <div className={classes.star_wrapper}>
                                    <div className={classes.star_progress}>
                                        <div className={classes.star_progress_active} style={{ width: `${rating.rate * 100 / 5}%` }}></div>
                                        <div className={classes.star_progress_passive}></div>
                                    </div>
                                    <StarHolderSVG className={classes.star_holder} />
                                </div>
                                : <StarHolderZeroSVG className={classes.icon_ex_star} />
                            }
                            {!!rating.votes ?
                                <>
                                    <div className={classes.rating_count}>{rating.rate}</div>
                                    <div className={classes.rating_text}><span>оцінок {rating.votes}</span></div>
                                </>
                                :
                                <div className={classes.rating_text_zero}><span>Оцініть першим</span></div>
                            }
                        </div>
                        <hr />
                        <div className={classes.product_weight}>{value}</div>
                    </div>
                </div>
                <ProductListItemControls {...props} />
            </div>
        </li >
    )
}

ProductListItem.propTypes = {
    available: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    rating: PropTypes.object.isRequired,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    favorite: PropTypes.arrayOf(PropTypes.object).isRequired,
    img: PropTypes.string,
    addProductsToFavorite: PropTypes.func,
}

export default ProductListItem;